import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import common_utility_package.Excel_Data_Reader;

public class dynamic_Driver_Class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		//Step1: Read the Test cases to be executed from the file
		ArrayList<String> TestCaseList = Excel_Data_Reader.Read_Excel_Data("API_data.xlsx", "TestCasesToExecute","TestCaseToExecute");
        System.out.println(TestCaseList);
        int count=TestCaseList.size();
        for (int i =1;i<count; i++)
        {
        	String TestCaseToExecute=TestCaseList.get(i);
        	System.out.println("Test Case Which is going to execute:"+TestCaseToExecute);
        	
        //Step2:Call the TestCaseToExecute on runtime by using java.lang.reflect package
        	Class<?> TestClass =Class.forName("testclasspackage."+TestCaseToExecute);
        
        	
        //Step3: Call the execute method of the class captures in variables in test class by using java.lang.reflect.method
        	Method ExecuteMethod=TestClass.getDeclaredMethod("executer");
        	
        //Step4: set the accessibility of method as true
        	ExecuteMethod.setAccessible(true);
        	
        //Step5:Create the instance of class captured in the test class variable
        	Object InstanceOfTestClass =TestClass.getDeclaredConstructor().newInstance();
        	
        //Step6: Execute the method captured in variable ExecuteMethod of class captured in TestClass variable
        	ExecuteMethod.invoke(InstanceOfTestClass);
        	System.out.println("Execution of Test Case Name:"+TestCaseToExecute+"...is Completed");
        	System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        	
        }

	}

}
