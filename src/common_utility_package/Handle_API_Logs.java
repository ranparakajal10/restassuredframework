package common_utility_package;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_Logs {
	public static File Create_Log_Directory(String Directory_name) {
		String Current_Project_Directory = System.getProperty("user.dir");
		System.out.println(Current_Project_Directory);
		File Log_Directory = new File(Current_Project_Directory + "//APILogs//Logs//" + Directory_name);
		Delete_Directory(Log_Directory);
		Log_Directory.mkdir();
		System.out.println(Log_Directory);
		return Log_Directory;
	}

	public static boolean Delete_Directory(File Directory) {
		boolean Directory_deleted = Directory.delete();
		{
			if (Directory.exists()) {
				File[] files = Directory.listFiles();
				if (files != null) {
					for (File file : files) {
						if (file.isDirectory()) {
							Delete_Directory(file);
						} else {
							file.delete();
						}
					}
				}
			}
			Directory_deleted = Directory.delete();
		}
		return Directory_deleted;
	}

	public static void Evidance_Creator(File Directory_name, String filename, String endpoint, String Requestbody,
			String Responsebody) throws IOException {
		// step1 create the file at given location
		File newfile = new File(Directory_name + "\\" + filename + ".txt");
		System.out.println("new file created to save evidence :" + newfile.getName());
		// step 2 write data into the file
		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("End point : " + endpoint + "\n\n");
		datawriter.write("Request Body : \n\n" + Requestbody + "\n\n");
		datawriter.write("Response Body: \n\n" + Responsebody);
		datawriter.close();
		System.out.println("EVIDENCE IS WRITTEN IN FILE : " + newfile.getName());

	}
}