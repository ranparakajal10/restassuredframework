package testclasspackage;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import common_method_package.Trigger_Api_Method;
import common_utility_package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;
import request_repository.Post_request_repository;

public class Post_TC1 extends Post_request_repository {
	@Test
	public static void executer() throws IOException
	{ 
		String Requestbody=post_TC1_Request();
       File Directory_name=Handle_API_Logs.Create_Log_Directory("Post_TC1");
		for(int i=0;i<5;i++)
		{
		int Status_Code = Trigger_Api_Method.extract_Status_Code(Requestbody, post_endpoint());
		//System.out.println("Status code:"+Status_Code);
		
		if(Status_Code==201)
		{
		String Responsebody = Trigger_Api_Method.extract_Responsebody(Requestbody, post_endpoint());
		System.out.println("ResponseBody:"+Responsebody);
	    Handle_API_Logs.Evidance_Creator(Directory_name,"Post_TC",post_endpoint(),Requestbody,Responsebody);
		Validator(Requestbody,Responsebody);
		break;
		}
		else
		{
			System.out.println("Desired Status code not found hence; retry");
		}
	
	}
	
	}
	public static void Validator(String Requestbody,String Responsebody) throws IOException
	{
        JsonPath jsp_req = new JsonPath(Requestbody);
		
		String req_name = jsp_req.getString("name");
		String req_job =jsp_req.getString("job");
		

		JsonPath jsp_res = new JsonPath(Responsebody);
		
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		
		LocalDateTime CurrentDate =LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0,11);
		String res_createdAt = jsp_res.getString("createdAt").substring(0,11);

		// Validation
		AssertJUnit.assertEquals(res_name, req_name);
		AssertJUnit.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		AssertJUnit.assertEquals(ExpectedDate, res_createdAt);
	}
}
