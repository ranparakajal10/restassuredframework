package testclasspackage;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_method_package.Trigger_Api_PutMethod;
import common_utility_package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;
import request_repository.Put_request_repository;

public class Put_TC1 extends Put_request_repository {
	@Test
	 public static void executer()throws IOException {
		 String Requestbody=put_TC1_Request();
		 File Directory_name=Handle_API_Logs.Create_Log_Directory("Put_TC1");
			for(int i=0;i<5;i++)
			{
			int Status_Code = Trigger_Api_PutMethod.extract_Status_Code(Requestbody, put_endpoint());
			System.out.println("Status code:"+Status_Code);
			
			if(Status_Code==200)
			{
			String Responsebody = Trigger_Api_PutMethod.putextract_Responsebody(Requestbody, put_endpoint());
			System.out.println("ResponseBody:"+Responsebody);
			//Handle_API_Logs.Evidance_Creator(Directory_name,"Put_TC",put_endpoint(),Requestbody,Responsebody);
			Validator(Requestbody,Responsebody);
			break;
			}
			else
			{
				System.out.println("Desired Status code not found hence; retry");
			}
		
		}
		}
		public static void Validator(String Requestbody,String Responsebody)
		{
			JsonPath jsp_req = new JsonPath(Requestbody);
			String req_name=jsp_req.getString("name");
			String req_job=jsp_req.getString("job");
			JsonPath jsp_res = new JsonPath(Responsebody);
			
			String res_name = jsp_res.getString("name");
			String res_job = jsp_res.getString("job");
			AssertJUnit.assertEquals(res_name, req_name);
			AssertJUnit.assertEquals(res_job, req_job);
			
	 }

	}



