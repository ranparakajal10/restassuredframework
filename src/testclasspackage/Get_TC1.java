package testclasspackage;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;
import common_method_package.Trigger_Api_GetMethod;
import common_utility_package.Handle_API_Logs;

public class Get_TC1 extends Trigger_Api_GetMethod {
	@Test
	public static void executer() throws IOException {
            File Directory_name=Handle_API_Logs.Create_Log_Directory("Get_TC1");
		for (int i = 0; i < 5; i++) {
			int Status_Code = Trigger_Api_GetMethod.extract_Status_Code(get_endpoint());
			System.out.println("Status code:" + Status_Code);

			if (Status_Code == 200) {
				String Responsebody = Trigger_Api_GetMethod.extract_Responsebody(get_endpoint());
				System.out.println("ResponseBody:" + Responsebody);
				Handle_API_Logs.Evidance_Creator(Directory_name,"Get_TC",get_endpoint(),"",Responsebody);
				Validator(Responsebody);
				break;
			} else {
				System.out.println("Desired Status code not found hence; retry");
			}

		}
	}

	public static void Validator(String Responsebody) {
		String[] Exp_id_array = { "7", "8", "9", "10", "11", "12" };
		String[] Exp_email_array = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				              "byron.fields@reqres.in","george.edwards@reqres.in", "rachel.howell@reqres.in"};
		String[] Exp_first_name_array = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel"};
		String[] Exp_last_name_array = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell"};
		String[] Exp_avatar_array = {"https://reqres.in/img/faces/7-image.jpg",
				"https://reqres.in/img/faces/8-image.jpg", "https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg", "https://reqres.in/img/faces/11-image.jpg",
				"https://reqres.in/img/faces/12-image.jpg"};
		JsonPath jsp_res = new JsonPath(Responsebody);
		// String res_page=jsp_res.getString("page");
		List<Object> res_data = jsp_res.getList("data");
		int count = res_data.size();
		for (int i = 0; i < count; i++) {
			String Exp_id = Exp_id_array[i];
			String res_id = jsp_res.getString("data[" + i + "].id");
			System.out.println(res_id);
			AssertJUnit.assertEquals(res_id, Exp_id);

			String Exp_email = Exp_email_array[i];
			String res_email = jsp_res.getString("data[" + i + "].email");
			System.out.println(res_email);
			AssertJUnit.assertEquals(res_email, Exp_email);

			String Exp_first_name = Exp_first_name_array[i];
			String res_first_name = jsp_res.getString("data[" + i + "].first_name");
			System.out.println(res_first_name);
			AssertJUnit.assertEquals(res_first_name, Exp_first_name);

			String Exp_last_name = Exp_last_name_array[i];
			String res_last_name = jsp_res.getString("data[" + i + "].last_name");
			System.out.println(res_last_name);
			AssertJUnit.assertEquals(res_last_name, Exp_last_name);

			String Exp_avatar = Exp_avatar_array[i];
			String res_avatar = jsp_res.getString("data[" + i + "].avatar");
			System.out.println(res_avatar);
			AssertJUnit.assertEquals(res_avatar, Exp_avatar);

		}

	}

}
