package common_method_package;

import static io.restassured.RestAssured.given;

import request_repository.Endpoint;

public class Trigger_Api_DeleteMethod extends Endpoint {

	public static int extract_Status_Code(String URL) {
		int StatusCode = given().header("Content-Type", "application/json").when().delete(URL).then().extract()
				.statusCode();
		return StatusCode;

	}

}
