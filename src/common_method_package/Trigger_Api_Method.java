package common_method_package;
import static io.restassured.RestAssured.given;

public class Trigger_Api_Method {
	public static int extract_Status_Code(String req_Body, String URL) {
		int StatusCode = given().header("Content-Type", "application/json").body(req_Body).when().post(URL).then()
				.extract().statusCode();
		return StatusCode;

	}

	public static String extract_Responsebody(String req_Body, String URL) {

		/*String Responsebody = given().header("Content-Type", "application/json").body(req_Body).log().all().when()
				.post(URL).then().log().all().extract().response().asString();
		return Responsebody;*/
		String Responsebody = given().header("Content-Type", "application/json").body(req_Body).when()
				.post(URL).then().extract().response().asString();
		return Responsebody;

	}
	
}
