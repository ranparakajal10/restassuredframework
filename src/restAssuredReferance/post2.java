package restAssuredReferance;
import io.restassured.*;
import static io.restassured.RestAssured.given;


public class post2 {

	public static void main(String[] args) {
		 String BaseURI="https://reqres.in/";
		 String requestbody="{\r\n"
		 		+ "    \"name\": \"morpheus\",\r\n"
		 		+ "    \"job\": \"leader\"\r\n"
		 		+ "}";
		 RestAssured.baseURI=BaseURI;
		 
		 given().header("Content-Type","application/json").body(requestbody).log().all()
			.when()
			.post("api/users")
			.then().log().all().extract().response().asString();
		  

	}

}
