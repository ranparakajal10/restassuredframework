package restAssuredReferance;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Postreferance1 
{
	public static void main(String[] args) {
	// step 1: Declare variables for Base URI and request body
     String BaseURI="https://reqres.in/";
     String Requestbody="{\r\n"
     		+ "    \"name\": \"morpheus\",\r\n"
     		+ "    \"job\": \"leader\"\r\n"
     		+ "}";
  // Step2 :Declare BaseURI
     RestAssured.baseURI=BaseURI;

     //step 3:Configure  the RequestBody and Trigger the Api
      String Responsebody=
    		  given().header("Content-Type","application/json").body(Requestbody)
    		  .when().post("/api/users")
    		  .then().extract().response().asString();
      // Step 4
      // Create an object of jsonpath to parse the requestbody
      JsonPath jsp_req= new JsonPath(Requestbody);
      String req_name=jsp_req.getString("name");
      System.out.println("Requestbody parameter Name:"+req_name);
      String req_job=jsp_req.getString("job");
      System.out.println("Requestbody parameter  Job:"+req_job);
      
     
   // Create an object of jsonpath to parse the responsebody
      JsonPath jsp_res= new JsonPath(Responsebody);
      String res_name=jsp_res.getString("name");
      System.out.println("Responsebody parameter Name:"+res_name);
      String res_job=jsp_res.getString("job");
      System.out.println("Responsebody parameter Job:"+res_job);
      String res_id=jsp_res.getString("id");
      System.out.println("Responsebody parameter id:"+res_id);
      String res_createdAt=jsp_res.getString("createdAt");
      System.out.println("Responsebody parameter Created At:"+res_createdAt);
      String generatedDate=res_createdAt.substring(0,10);
      System.out.println(generatedDate);
      LocalDateTime currentDate = LocalDateTime.now();
      System.out.println(currentDate);
      String ExpectedDate=currentDate.toString().substring(0,10);
      System.out.println(ExpectedDate);
     // step 5:Validate the responsebody parameters
      Assert.assertEquals(res_name, req_name);
      Assert.assertEquals(res_job, req_job);
      Assert.assertNotNull(res_id);
      Assert.assertEquals(generatedDate,ExpectedDate);
   }
}
