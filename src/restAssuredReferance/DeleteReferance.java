package restAssuredReferance;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;


public class DeleteReferance {

	public static void main(String[] args) {
	// step 1: Declare variables for Base URI and request body
	String BaseURI="https://reqres.in/";
			
	  // Step2 :Declare BaseURI
	
	  RestAssured.baseURI=BaseURI;
	  
	  //step 3:Configure  the RequestBody and Trigger the Api
		
	  given().log().all().when().delete("/api/users/2")
	
		.then().log().all().extract().response().asString();
}
}
