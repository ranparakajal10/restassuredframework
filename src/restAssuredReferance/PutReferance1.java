package restAssuredReferance;
import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
public class PutReferance1 {

	public static void main(String[] args) {
		// step 1: Declare variables for Base URI and request body
	     String BaseURI="https://reqres.in/";
	     String Requestbody="{\r\n"
	     		+ "    \"name\": \"morpheus\",\r\n"
	     		+ "    \"job\": \"zion resident\"\r\n"
	     		+ "}";
	  // Step2 :Declare BaseURI
	     RestAssured.baseURI=BaseURI;

	     //step 3:Configure  the RequestBody and Trigger the Api
	      String Responsebody=
	    		  given().header("Content-Type","application/json").body(Requestbody)
	    		  .when().put("/api/users/2")
	    		  .then().extract().response().asString();
	      // Step 4
	      // Create an object of jsonpath to parse the requestbody
	      JsonPath jsp_req= new JsonPath(Requestbody);
	      String req_name=jsp_req.getString("name");
	      System.out.println("Requestbody parameter Name:"+req_name);
	      String req_job=jsp_req.getString("job");
	      System.out.println("Requestbody parameter  Job:"+req_job);
	     
	   // Create an object of jsonpath to parse the responsebody
	      JsonPath jsp_res= new JsonPath(Responsebody);
	      String res_name=jsp_res.getString("name");
	      System.out.println("Responsebody parameter Name:"+res_name);
	      String res_job=jsp_res.getString("job");
	      System.out.println("Responsebody parameter Job:"+res_job);
	      String res_updatedAt=jsp_res.getString("updatedAt");
	      System.out.println("Responsebody parameter updated At:"+res_updatedAt);
	      String generatedDate=res_updatedAt.substring(0,10);
	      System.out.println(generatedDate);
	      LocalDateTime currentDate = LocalDateTime.now();
	      System.out.println(currentDate);
	      String ExpectedDate=currentDate.toString().substring(0,10);
	      System.out.println(ExpectedDate);
	      
	     // step 5:Validate the responsebody parameters
	      Assert.assertEquals(res_name, req_name);
	      Assert.assertEquals(res_job, res_job);
	      Assert.assertEquals(generatedDate,ExpectedDate);
	 }

}


