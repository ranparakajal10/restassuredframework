package restAssuredReferance;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class GetReferance1 {

	public static void main(String[] args) {
		// step 1: Declare variables for Base URI and request body
	     String BaseURI="https://reqres.in/";
	     
	  // Step2 :Declare BaseURI
	     RestAssured.baseURI=BaseURI;

	     //step 3:Configure  the RequestBody and Trigger the Api
	      String Responsebody=
	    		  given().header("Content-Type","application/json")
	    		  .when().get("/api/users?page=2")
	    		  .then().extract().response().asString();
	     int[] req_id = {7,8,9,10,11,12};
	     String[] req_email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
					"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
			String[] req_first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
			String[] req_last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
			String[] req_avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
					"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
					"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

       
	   // Create an object of jsonpath to parse the responsebody
	      JsonPath jsp_res= new JsonPath(Responsebody);
	      int per_page =jsp_res.getInt("per_page");
	      for(int i=0 ;i<per_page ;i++)
	      {
	    	 int  actualid=jsp_res.getInt("data[ "+i+ "].id");
	    	 String  actualEmail=jsp_res.getString("data[" +i+ "].email");
	    	 String  actualfirst_name=jsp_res.getString("data[" +i+ "].first_name");
	    	 String  actuallast_name=jsp_res.getString("data[" +i+ "].last_name");
	    	 String  actualavatar=jsp_res.getString("data[" +i+ "].avatar");
	    	 
	    	 int expectedid=req_id[i];
	    	 String expectedEmail=req_email[i];
	    	 String expectedfirstname=req_first_name[i];
	    	 String expectedlastname=req_last_name[i];
	    	 String expectedavatar=req_avatar[i];
	    	 
	    	 Assert.assertEquals(actualid,expectedid);
	    	 Assert.assertEquals(actualEmail,expectedEmail);
	    	 Assert.assertEquals(actualfirst_name,expectedfirstname);
	    	 Assert.assertEquals(actuallast_name,expectedlastname);
	    	 Assert.assertEquals(actualavatar,expectedavatar);
	    	 
	    	 System.out.println("Validation successful for data entry " + i);
			}
	    	  
	      }
	      
	}


