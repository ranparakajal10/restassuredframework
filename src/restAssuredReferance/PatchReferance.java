package restAssuredReferance;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;

public class PatchReferance {

	public static void main(String[] args) {
	// step 1: Declare variables for Base URI and request body
		String BaseURI="https://reqres.in/";
		String Requestbody=" {\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		
  // Step2 :Declare BaseURI
  RestAssured.baseURI=BaseURI;
  //step 3:Configure  the RequestBody and Trigger the Api
  /*String responsebody=given().header("Content-Type","application/json").body(Requestbody).when().post("api/users")
	.then().extract().response().asString();
	System.out.println(responsebody);*/
	
  given().header("Content-Type","application/json").body(Requestbody).log().all()
	.when()
	.patch("api/users/2")
	.then().log().all().extract().response().asString();
	}

}
