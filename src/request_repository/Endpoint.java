package request_repository;

public class Endpoint 
{

	static String hostname = "https://reqres.in/";
	
	public static String post_endpoint() {
		String URL = hostname+"api/users";
		//System.out.println(URL);
		return URL;
	}
	public static String patch_endpoint() {
		String URL = hostname+"api/users/2";
		System.out.println(URL);
		return URL;
	}
	public static String put_endpoint() {
		String URL = hostname+"api/users/2";
		System.out.println(URL);
		return URL;
	}
	public static String get_endpoint() {
		String URL = hostname+"api/users?page=2";
		System.out.println(URL);
		return URL;
	}
	public static String delete_endpoint() {
		String URL = hostname+"api/users/2";
		System.out.println(URL);
		return URL;
	}
}
