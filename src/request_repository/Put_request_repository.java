package request_repository;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_Data_Reader;

public class Put_request_repository extends Endpoint
{
	public static String put_TC1_Request() throws IOException
	{

		ArrayList<String> excelData=Excel_Data_Reader.Read_Excel_Data("API_data.xlsx", "PUT_API", "Put_Tc_2");
             String req_name=excelData.get(1);
             String res_job=excelData.get(2);
             
		String Requestbody="{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+res_job+"\"\r\n"
				+ "}";
		return Requestbody;
	}

}
